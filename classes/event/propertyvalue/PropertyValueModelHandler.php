<?php namespace Lovata\Shbasecode\Classes\Event\PropertyValue;

use Lovata\PropertiesShopaholic\Models\PropertyValue;

/**
 * Class PropertyModelHandler
 * @package Lovata\Shbasecode\Classes\Event\PropertyValue
 * @author Andrey Kharanenka, a.khoronenko@lovata.com, LOVATA Group
 */
class PropertyValueModelHandler
{
     /**
     * Add listeners
     */
    public function subscribe()
    {
        PropertyValue::extend(function($obPropertyValue){
            /** @var PropertyValue $obPropertyValue */
            $obPropertyValue->attachOne['icon'] = 'System\Models\File';
            $obPropertyValue->fillable[] = 'color';
            $obPropertyValue->addCachedField(['icon', 'color']);
        });
    }
}

