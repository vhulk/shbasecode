<?php namespace Lovata\Shbasecode\Classes\Event\PropertyValue;

use Lang;
use Lovata\Shopaholic\Models\Measure;
use Lovata\Shopaholic\Models\Setting;
use Lovata\Toolbox\Classes\Event\AbstractBackendFieldHandler;


use Lovata\PropertiesShopaholic\Models\PropertyValue;
use Lovata\PropertiesShopaholic\Controllers\PropertyValues;

/**
 * Class ExtendPropetyFieldsHandler
 * @package Lovata\Shbasecode\Classes\Event\PropertyValue
 */

class ExtendPropertyValueFieldsHandler extends AbstractBackendFieldHandler
{
  protected $iPriority = 15000;

  /**
     * Extend form fields
     * @param \Backend\Widgets\Form $obWidget
     */
    protected function extendFields($obWidget)
    {
        $arAdditionFields = [
            'icon' => [
                'label' => 'Icon',
                'type' => 'fileupload',
                'mode' => 'image'
            ],
            'color' => [
                'label' => "Цвет",
                'type' => 'colorpicker',
            ]
        ];
        $obWidget->addTabFields($arAdditionFields);
    }

    /**
     * Get model class name
     * @return string
     */
    protected function getModelClass() : string
    {
        return PropertyValue::class;
    }

    /**
     * Get controller class name
     * @return string
     */
    protected function getControllerClass() : string
    {
        return PropertyValues::class;
    }
}
